package com.example.admin.epicerielist;



/**
 * Created by Admin on 2018-01-24.
 */

public class Produit {
    private String nom;
    private double prix;

    // Construxtor
    public  Produit(String name, double price)
    {
        super();
       this.nom = name;
       this.prix = price;
    }
    // Default Constructor
    public Produit()
    {
        super();
        this.nom="no name";
        this.prix=0;
    }
    public String getNom(){return this.nom;}
    public double getPrix(){return this.prix;}
    public String toString(){return this.nom + " : " + this.prix + " $";}
}
