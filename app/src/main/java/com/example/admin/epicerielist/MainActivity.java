package com.example.admin.epicerielist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Currency;

public class MainActivity extends AppCompatActivity {
    private EditText NomProduitTXT, PrixProduitTXT;
    private ListView ShoppingListView;
    private TextView Totalelbl;
    private ArrayAdapter<Produit> ListShoppingAdapter;
    private ArrayList<Produit> ProduitArray;
    private double totale ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NomProduitTXT = (EditText)findViewById(R.id.nomProduitTXT);
        PrixProduitTXT = (EditText)findViewById(R.id.PrixProduitTXT);
        ShoppingListView = (ListView)findViewById(R.id.EpicerieList);
        Totalelbl = (TextView)findViewById(R.id.Totalelbl);

        ProduitArray = new ArrayList<Produit>();
        ListShoppingAdapter = new ArrayAdapter<Produit>(this,android.R.layout.simple_list_item_1,ProduitArray);

        ShoppingListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                totale-= calculateTax(ProduitArray.get(i).getPrix());
                Totalelbl.setText(totale + " $");
                ProduitArray.remove(i);
                ListShoppingAdapter.notifyDataSetChanged();
                return true;
            }
        });

    }

    public void AddProduit(View view) {
        if(NomProduitTXT.getText().toString().matches("") || PrixProduitTXT.getText().toString().matches(""))
        {
            Toast.makeText(getApplicationContext(),"Entrez un nom et un prix exactes",Toast.LENGTH_SHORT).show();
        }
        else
            {
                Produit TempProd =  new Produit(NomProduitTXT.getText().toString(), Double.parseDouble(PrixProduitTXT.getText().toString()));
                ProduitArray.add(TempProd);
                totale += calculateTax(TempProd.getPrix());
                Totalelbl.setText(totale + " $");
                ListShoppingAdapter.notifyDataSetChanged();
                ShoppingListView.setAdapter(ListShoppingAdapter);
                NomProduitTXT.setText("");
                PrixProduitTXT.setText("");
            }

    }
    private double calculateTax(double p){
        return p + (p*.015);
    }

}
